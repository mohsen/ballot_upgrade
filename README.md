# Ballot_upgrade

The upgrade of Ballot solidity tutorial example code.(in the [website](https://docs.soliditylang.org/en/latest/solidity-by-example.html))

## TODO

- [x] Bug fixing
- [ ] Performance upgrade
- [ ] Add new features

## Bug fixing

- Bug 0: it fix the delegating from adderess has wight 0. explanation: when an address has wight 0 (Has no right to vote) can delegate his vote to others, and therefore his vote burns (voted = true) because after creator make right to him to vote (make wigth = 1), he cann't to vote.

## Test and Deploy

Put it in the Remix-IDE and test . . .


## Project status

In progress . . .
